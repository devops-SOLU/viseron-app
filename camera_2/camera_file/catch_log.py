import logging
import sys

def infoLog(message):
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.INFO,
        format='%(asctime)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S',
    )
    logging.info(message)

def errorLog(message):
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.ERROR,
        format='%(asctime)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S',
        
        )
    logging.error(message)