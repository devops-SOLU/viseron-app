from camera_file import camera_2
import asyncio
import os

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5002))
    asyncio.run(camera_2.main())