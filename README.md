# Engine Camera

# Introduction

This is a simple engine camera for CCTV in PLTG and PLTU muara karang. This engine is usefull for monitoring the engine room in real time. This engine is using OpenCV and Python.

# Installation docker
- For installation docker you can follow this link https://docs.docker.com/engine/install/ubuntu/
- For installation docker-compose you can follow this link https://docs.docker.com/compose/install/

# Requirements
- Viseron
- Compreface

# Installation
- First you have to create file static inside every file camera1, camera2, camera3 and camera4
- The folder static is inside the camera_file
- You have to install docker and docker-compose

# How to run
- You have to run docker-compose up -d --build
- Open your browser and type localhost:9443 to see the container is already running or not


