from websocket import create_connection
import json
import asyncio
import requests
import time
import subprocess as sp
import datetime
import os
from . import catch_log
import cv2

# CAMERA PLTG OUT

url = "http://172.16.14.140:8096/api/ppe-log"

def take_snapshot(rtsp_url, folder_path, image_name, quality=75):
    # Create a VideoCapture object and open the RTSP stream
    cap = cv2.VideoCapture(rtsp_url)

    # Read a frame from the video stream
    ret, frame = cap.read()

    # Check if a frame was captured successfully
    if ret:
        # Create the folder if it doesn't exist
        if not os.path.exists(folder_path):
            try:
                os.makedirs(folder_path)
            except any as e :
                catch_log.errorLog('No create folder {}'.format(e))

        # Save the frame as an image file in the specified folder
        try:
            cv2.imwrite(os.path.join(folder_path, image_name), frame, [cv2.IMWRITE_JPEG_QUALITY, quality])
            catch_log.infoLog('Success write file')
        except any as e :
            catch_log.errorLog('Gagal bikin file {}'.format(e))

    # Release the VideoCapture object
    cap.release()

def get_data():

    result_face = None
    object_detected = []
    result_get_object_sh = None
    result_get_object_ss = None
    result_get_face = None
    result_face_detected = None

    ws = create_connection("ws://localhost:8888/websocket")
    ws.send(json.dumps({"type":"get_entities","command_id":5}))
    
    result_5 = ws.recv()
    data_result_5 = json.loads(result_5)
    
    while True:

        result_key_face = data_result_5['result']
        result_key_helmet = data_result_5['result']['binary_sensor.fr_pltu_out_object_detected_safety_helmet']
        result_key_shoes = data_result_5['result']['binary_sensor.ppe_pltu_out_object_detected_safety_shoes']

        for value, entity_id in result_key_face.items():
            if 'binary_sensor.fr_pltu_out_face_detected' in value:
                if entity_id['state'] == 'on':
                    result_get_face = entity_id['attributes']['name']
                    catch_log.infoLog('Face detected : ' + result_get_face)
                    break

                # else:
                #     catch_log.infoLog("Face not detected.")
        
        if result_key_helmet['state'] == 'on':
            result_get_object_sh = result_key_helmet["attributes"]["objects"][0]["label"]
            catch_log.infoLog(result_get_object_sh)
        else:
            catch_log.infoLog("Safety helmet not detected.")

        if result_key_shoes['state'] == 'on':
            result_get_object_ss = result_key_shoes['attributes']["objects"][0]["label"]
            catch_log.infoLog(result_get_object_ss)
        else:
            catch_log.errorLog("Safety shoes not detected.")
        
        if result_get_face is not None and result_get_object_sh is not None and result_get_object_ss is not None:
            ws.close()
            time.sleep(2)
        else:
            ws.close()
            catch_log.errorLog("Face, Safety helmet, and Safety shoes not detected.")
            pass
            
        if result_get_object_sh is not None:
            object_detected.append(result_get_object_sh)
        else:
            pass

        if result_get_object_ss is not None:
            object_detected.append(result_get_object_ss)
        else:
            pass

        if result_get_face is not None:
            result_face_detected = result_get_face
        else:
            pass


        if result_face_detected == None and object_detected == ['safety_helmet', 'safety_shoes']:
            catch_log.infoLog("Face Detected: {}".format(result_face_detected))
            catch_log.infoLog("Object Detected: {}".format(object_detected))

            now = datetime.datetime.now()
            rtsp_url_face = "rtsp://admin:solu5758@172.16.14.138:554/live"
            folder_path_face = "/camera_4/camera_file/static/img/faces"
            image_name_face = "fr_{}_{}.jpg".format(result_face_detected, now)
            quality = 50

            rtsp_url_fullbody = "rtsp://admin:solu5758@172.16.14.137:554/Streaming/Channels/101:554/Streaming/Channels/101"
            folder_path_fullbody = "/camera_4/camera_file/static/img/fullbody"
            image_name_fullbody = "ppe_{}_{}.jpg".format(result_face_detected, now)
            quality = 50

            payload = {
                "subject_name" : result_face_detected,
                "face_foto_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/faces/{}'.format(image_name_face),
                "fullbody_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/fullbody/{}'.format(image_name_fullbody),
                "is_complete" : 0,
                "camera_gate_id" : 4,
                "helmets" : 1,
                "boots" : 1,
            }
            try:
                response = requests.request("POST", url, data=payload)
                response.raise_for_status()
                catch_log.infoLog(f'Post data to server is success : {response.status_code}')
            except requests.exceptions.RequestException as e :
                catch_log.errorLog(f'Post data to server is failed : {e}')
                pass
            take_snapshot(rtsp_url=rtsp_url_face, folder_path=folder_path_face, image_name=image_name_face, quality=quality)
            take_snapshot(rtsp_url=rtsp_url_fullbody, folder_path=folder_path_fullbody, image_name=image_name_fullbody, quality=quality)
            time.sleep(5)
        elif result_face_detected == None and object_detected == ['safety_helmet']:
            catch_log.infoLog("Face Detected: {}".format(result_face_detected))
            catch_log.infoLog("Object Detected: {}".format(object_detected))

            now = datetime.datetime.now()
            rtsp_url_face = "rtsp://admin:solu5758@172.16.14.138:554/live"
            folder_path_face = "/camera_4/camera_file/static/img/faces"
            image_name_face = "fr_{}_{}.jpg".format(result_face_detected, now)
            quality = 50

            rtsp_url_fullbody = "rtsp://admin:solu5758@172.16.14.137:554/Streaming/Channels/101:554/Streaming/Channels/101"
            folder_path_fullbody = "/camera_4/camera_file/static/img/fullbody"
            image_name_fullbody = "ppe_{}_{}.jpg".format(result_face_detected, now)
            quality = 50

            payload = {
                "subject_name" : result_face_detected,
                "face_foto_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/faces/{}'.format(image_name_face),
                "fullbody_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/fullbody/{}'.format(image_name_fullbody),
                "is_complete" : 0,
                "camera_gate_id" : 4,
                "helmets" : 1,
                "boots" : 0,
            }
            try:
                response = requests.request("POST", url, data=payload)
                response.raise_for_status()
                catch_log.infoLog(f'Post data to server is success : {response.status_code}')
            except requests.exceptions.RequestException as e :
                catch_log.errorLog(f'Post data to server is failed : {e}')
                pass
            take_snapshot(rtsp_url=rtsp_url_face, folder_path=folder_path_face, image_name=image_name_face, quality=quality)
            take_snapshot(rtsp_url=rtsp_url_fullbody, folder_path=folder_path_fullbody, image_name=image_name_fullbody, quality=quality)
            time.sleep(5)
        elif result_face_detected == None and object_detected == ['safety_shoes']:
            catch_log.infoLog("Face Detected: {}".format(result_face_detected))
            catch_log.infoLog("Object Detected: {}".format(object_detected))

            now = datetime.datetime.now()
            rtsp_url_face = "rtsp://admin:solu5758@172.16.14.138:554/live"
            folder_path_face = "/camera_4/camera_file/static/img/faces"
            image_name_face = "fr_{}_{}.jpg".format(result_face_detected, now)
            quality = 50

            rtsp_url_fullbody = "rtsp://admin:solu5758@172.16.14.137:554/Streaming/Channels/101:554/Streaming/Channels/101"
            folder_path_fullbody = "/camera_4/camera_file/static/img/fullbody"
            image_name_fullbody = "ppe_{}_{}.jpg".format(result_face_detected, now)
            quality = 50

            payload = {
                "subject_name" : result_face_detected,
                "face_foto_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/faces/{}'.format(image_name_face),
                "fullbody_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/fullbody/{}'.format(image_name_fullbody),
                "is_complete" : 0,
                "camera_gate_id" : 4,
                "helmets" : 0,
                "boots" : 1,
            }
            try:
                response = requests.request("POST", url, data=payload)
                response.raise_for_status()
                catch_log.infoLog(f'Post data to server is success : {response.status_code}')
            except requests.exceptions.RequestException as e :
                catch_log.errorLog(f'Post data to server is failed : {e}')
                pass
            take_snapshot(rtsp_url=rtsp_url_face, folder_path=folder_path_face, image_name=image_name_face, quality=quality)
            take_snapshot(rtsp_url=rtsp_url_fullbody, folder_path=folder_path_fullbody, image_name=image_name_fullbody, quality=quality)
            time.sleep(5)
        elif result_face_detected == None and object_detected == []:
            catch_log.infoLog("Face Detected: {}".format(result_face_detected))
            catch_log.infoLog("Object Detected: {}".format(object_detected))
        else:
            if all(x in object_detected for x in ["safety_helmet", "safety_shoes"]):
                catch_log.infoLog("Face Detected: {}".format(result_face_detected))
                catch_log.infoLog("Object Detected: {}".format(object_detected))

                now = datetime.datetime.now()
                rtsp_url_face = "rtsp://admin:solu5758@172.16.14.138:554/live"
                folder_path_face = "/camera_4/camera_file/static/img/faces"
                image_name_face = "fr_{}_{}.jpg".format(result_face_detected, now)
                quality = 50

                rtsp_url_fullbody = "rtsp://admin:solu5758@172.16.14.137:554/Streaming/Channels/101:554/Streaming/Channels/101"
                folder_path_fullbody = "/camera_4/camera_file/static/img/fullbody"
                image_name_fullbody = "ppe_{}_{}.jpg".format(result_face_detected, now)
                quality = 50

                payload = {
                    "subject_name" : result_face_detected,
                    "face_foto_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/faces/{}'.format(image_name_face),
                    "fullbody_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/fullbody/{}'.format(image_name_fullbody),
                    "is_complete" : 1,
                    "camera_gate_id" : 4,
                    "helmets" : 1,
                    "boots" : 1,
                }
                try:
                    response = requests.request("POST", url, data=payload)
                    response.raise_for_status()
                    catch_log.infoLog(f'Post data to server is success : {response.status_code}')
                except requests.exceptions.RequestException as e :
                    catch_log.errorLog(f'Post data to server is failed : {e}')
                    pass
                take_snapshot(rtsp_url=rtsp_url_face, folder_path=folder_path_face, image_name=image_name_face, quality=quality)
                take_snapshot(rtsp_url=rtsp_url_fullbody, folder_path=folder_path_fullbody, image_name=image_name_fullbody, quality=quality)
                time.sleep(5)

            elif len(object_detected) == 1:
                if "safety_helmet" in object_detected:
                    catch_log.infoLog("Face Detected: {}".format(result_face_detected))
                    catch_log.infoLog("Object Detected: {}".format(object_detected))
                    now = datetime.datetime.now()
                    rtsp_url_face = "rtsp://admin:solu5758@172.16.14.138:554/live"
                    folder_path_face = "/camera_4/camera_file/static/img/faces"
                    image_name_face = "fr_{}_{}.jpg".format(result_face_detected, now)
                    quality = 50

                    rtsp_url_fullbody = "rtsp://admin:solu5758@172.16.14.137:554/Streaming/Channels/101:554/Streaming/Channels/101"
                    folder_path_fullbody = "/camera_4/camera_file/static/img/fullbody"
                    image_name_fullbody = "ppe_{}_{}.jpg".format(result_face_detected, now)
                    quality = 50

                    payload = {
                        "subject_name" : result_face_detected,
                        "face_foto_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/faces/{}'.format(image_name_face),
                        "fullbody_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/fullbody/{}'.format(image_name_fullbody),
                        "is_complete" : 1,
                        "camera_gate_id" : 4,
                        "helmets" : 1,
                        "boots" : 1,
                    }
                    try:
                        response = requests.request("POST", url, data=payload)
                        response.raise_for_status()
                        catch_log.infoLog(f'Post data to server is success : {response.status_code}')
                    except requests.exceptions.RequestException as e :
                        catch_log.errorLog(f'Post data to server is failed : {e}')
                        pass
                    take_snapshot(rtsp_url=rtsp_url_face, folder_path=folder_path_face, image_name=image_name_face, quality=quality)
                    take_snapshot(rtsp_url=rtsp_url_fullbody, folder_path=folder_path_fullbody, image_name=image_name_fullbody, quality=quality)
                    time.sleep(5)

                if "safety_shoes" in object_detected:
                    catch_log.infoLog("Face Detected: {}".format(result_face_detected))
                    catch_log.infoLog("Object Detected: {}".format(object_detected))
                    now = datetime.datetime.now()
                    rtsp_url_face = "rtsp://admin:solu5758@172.16.14.138:554/live"
                    folder_path_face = "/camera_4/camera_file/static/img/faces"
                    image_name_face = "fr_{}_{}.jpg".format(result_face_detected, now)
                    quality = 50

                    rtsp_url_fullbody = "rtsp://admin:solu5758@172.16.14.137:554/Streaming/Channels/101:554/Streaming/Channels/101"
                    folder_path_fullbody = "/camera_4/camera_file/static/img/fullbody"
                    image_name_fullbody = "ppe_{}_{}.jpg".format(result_face_detected, now)
                    quality = 50

                    payload = {
                        "subject_name" : result_face_detected,
                        "face_foto_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/faces/{}'.format(image_name_face),
                        "fullbody_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/fullbody/{}'.format(image_name_fullbody),
                        "is_complete" : 1,
                        "camera_gate_id" : 4,
                        "helmets" : 1,
                        "boots" : 1,
                    }
                    try:
                        response = requests.request("POST", url, data=payload)
                        response.raise_for_status()
                        catch_log.infoLog(f'Post data to server is success : {response.status_code}')
                    except requests.exceptions.RequestException as e :
                        catch_log.errorLog(f'Post data to server is failed : {e}')
                        pass
                    take_snapshot(rtsp_url=rtsp_url_face, folder_path=folder_path_face, image_name=image_name_face, quality=quality)
                    take_snapshot(rtsp_url=rtsp_url_fullbody, folder_path=folder_path_fullbody, image_name=image_name_fullbody, quality=quality)
                    time.sleep(5)
            else:
                catch_log.infoLog("Face Detected: {}".format(result_face_detected))
                catch_log.infoLog("Object Detected: {}".format(object_detected))

                now = datetime.datetime.now()
                rtsp_url_face = "rtsp://admin:solu5758@172.16.14.138:554/live"
                folder_path_face = "/camera_4/camera_file/static/img/faces"
                image_name_face = "fr_{}_{}.jpg".format(result_face_detected, now)
                quality = 50

                rtsp_url_fullbody = "rtsp://admin:solu5758@172.16.14.137:554/Streaming/Channels/101:554/Streaming/Channels/101"
                folder_path_fullbody = "/camera_4/camera_file/static/img/fullbody"
                image_name_fullbody = "ppe_{}_{}.jpg".format(result_face_detected, now)
                quality = 50

                payload = {
                    "subject_name" : result_face_detected,
                    "face_foto_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/faces/{}'.format(image_name_face),
                    "fullbody_url" : 'localhost:8096/assets/camera_pltu_out/camera_file/static/img/fullbody/{}'.format(image_name_fullbody),
                    "is_complete" : 1,
                    "camera_gate_id" : 4,
                    "helmets" : 1,
                    "boots" : 1,
                }
                try:
                    response = requests.request("POST", url, data=payload)
                    response.raise_for_status()
                    catch_log.infoLog(f'Post data to server is success : {response.status_code}')
                except requests.exceptions.RequestException as e :
                    catch_log.errorLog(f'Post data to server is failed : {e}')
                    pass
                take_snapshot(rtsp_url=rtsp_url_face, folder_path=folder_path_face, image_name=image_name_face, quality=quality)
                take_snapshot(rtsp_url=rtsp_url_fullbody, folder_path=folder_path_fullbody, image_name=image_name_fullbody, quality=quality)
                time.sleep(5)
        break
        
async def main():
    while True:
        get_data()
        catch_log.infoLog(f'Server is running')