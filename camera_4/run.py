from camera_file import camera_4
import asyncio
import os

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5004))
    asyncio.run(camera_4.main())