from camera_file import camera_3
import asyncio
import os

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5003))
    asyncio.run(camera_3.main())