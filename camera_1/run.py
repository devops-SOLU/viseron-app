from camera_file import cam1
import asyncio
import os

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5001))
    asyncio.run(cam1.main())